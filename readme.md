This is a simple project showcasing a static website running in a docker container using nginx.

To get started, clone the repo locally, cd into the project folder, then run the following docker compose command:
(sudo) docker-compose -f docker-compose.yml up -d

Keep in mind, the port is currently set to 90 in the docker-compose.yml file. Make sure that port 90 is not occupied by any other services, or else it won't work. Alternitavely, edit the docker-compose.yml file and configure a different port.
